<?php
/*
 * @author Luiz Fernando Lidio | The New Black Studio
 * @powered by: http://www.thenewblack.me
 * 
 *            .=     ,        =.
 *   _  _   /'/    )\,/,/(_   \ \
 *    `//-.|  (  ,\\)\//\)\/_  ) |
 *    //___\   `\\\/\\/\/\\///'  /
 * ,-"~`-._ `"--'_   `"""`  _ \`'"~-,_      Múúúúúúúúúúúúúúú!
 * \       `-.  '_`.      .'_` \ ,-"~`/     Hier gibt's nichts zu sehen!!!
 *  `.__.-'`/   (-\        /-) |-.__,'
 *    ||   |     \O)  /^\ (O/  |
 *    `\\  |         /   `\    /
 *      \\  \       /      `\ /
 *       `\\ `-.  /' .---.--.\
 *         `\\/`~(, '()      ('
 *          /(O) \\   _,.-.,_)
 *         //  \\ `\'`      /
 *        / |  ||   `""""~"`
 *      /'  |__||
 *             `o 
 * 
 * 
 */

/*
 **************************************************
 **************************************************
 REMOVE METABOX POSTS AND PAGES 
 **************************************************
 **************************************************
 */

if (is_admin()) :
function my_remove_meta_boxes() {
    remove_meta_box('pageparentdiv', 'page', 'side');
    remove_meta_box('authordiv', 'page', 'side');
    remove_meta_box('postcustom', 'page', 'side');
    remove_meta_box('pageparentdiv', 'post', 'side');
    remove_meta_box('authordiv', 'post', 'side');
    remove_meta_box('postcustom', 'post', 'side');
    remove_meta_box('formatdiv', 'post', 'side');
    remove_meta_box('tagsdiv-post_tag', 'post', 'side');
    remove_meta_box('postexcerpt', 'post', 'side');
    remove_meta_box('se-metabox', 'post', 'side');
}
add_action( 'admin_menu', 'my_remove_meta_boxes' );
endif;

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
  	#toplevel_page_edit-post_type-acf{ display:none;}
    #menu-users{ display:none;}
    #toplevel_page_wpcf7{ display:none;}
    } 
  </style>';
}

/*
 **************************************************
 **************************************************
 MENU ITENS
 **************************************************
 **************************************************
 */

function remove_menus () {
global $menu;
//    $restricted = array(__('Dashboard'), __('Posts'), __('Links'), __('Appearance'), __('Tools'), __('Settings'), __('Comments'), __('Plugins'), __('acf'));
    $restricted = array();
    end ($menu);
    while (prev($menu)){
        $value = explode(' ',$menu[key($menu)][0]);
        if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
    }
}
add_action('admin_menu', 'remove_menus');

add_filter('acf/settings/show_admin', '__return_false');

// Removendo o footer do admin

function remove_footer_admin () {
    echo "<a href='http://thenewblack.me' target='_blank'>Desenvolvido por The New Black</a>";
} 
add_filter('admin_footer_text', 'remove_footer_admin'); 

// Desabilitando auto-update

define( 'AUTOMATIC_UPDATER_DISABLED', true );
define( 'WP_AUTO_UPDATE_CORE', false );

add_action( 'admin_init', 'no_update_nag' );
function no_update_nag() {
	remove_action( 'admin_notices', 'update_nag', 3 );
}

function replace_footer_admin ()
{
echo '<span id="footer-thankyou"></span>';

}
add_filter('admin_footer_text', 'replace_footer_admin');
function replace_footer_version()
{
return '';
}
add_filter( 'update_footer', 'replace_footer_version', '1234');