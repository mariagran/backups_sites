<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header>
		oiiioio
		<h2><a href="<?php the_permalink(); ?>">  <?php the_title(); ?></a></h2>
	</header>
	<?php if ( has_post_thumbnail()) { ?>
		<div class="categoriaContent clearfix">
			<div class="large-2 columns">
				   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
				   <?php if ( has_post_thumbnail() ) {
				   		the_post_thumbnail('thumbnail', array('class' => 'th'));
				   	} ?>
				   </a>
			</div>
			<div class="large-10 columns">
				<?php the_excerpt() ?>
			</div>
		</div>
		<?php } else{ ?>
		<div class="categoriaContentSemFoto clearfix">
			<div class="large-12">
				<?php the_excerpt() ?>
			</div>
		</div>
		<?php }?>
</article>