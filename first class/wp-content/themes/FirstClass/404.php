<?php get_header(); ?>

<!-- Row for main content area -->
	<div class="row">
		<div class="small-12 large-12 columns" role="main">

				    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
						<header>
							<h1 class="entry-title"><?php _e('Não encontrado', 'FoundationPress'); ?></h1>
						</header>
						<div class="entry-content">
							<div class="error">
								<p class="bottom"><?php _e('A página que você está procurando pode ter sido removida, teve seu nome alterado ou está temporariamente indisponível.', 'FoundationPress'); ?></p>
							</div>
							<p><?php _e('Por favor, tente o seguinte:', 'FoundationPress'); ?></p>
							<ul> 
								<li><?php _e('Verifique a ortografia', 'FoundationPress'); ?></li>
								<li><?php printf(__('Retorne para a <a href="%s">página principal</a>', 'FoundationPress'), home_url()); ?></li>
								<li><?php _e('<a href="javascript:history.back()">Volte para a página anterior</a>', 'FoundationPress'); ?></li>
							</ul>
						</div>
					</article>

		</div>
	</div>
<?php get_footer(); ?>