<?php
/**
 * The template for displaying a "No posts found" message
 *
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */
?>

<header class="page-header">
	<h1 class="page-title"><?php _e( 'Não encontrado', 'FoundationPress' ); ?></h1>
</header>

<div class="page-content">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

	<p><?php printf( __( 'Pronto para publicar o seu primeiro post? <a href="%1$s">Clique aqui</a>.', 'FoundationPress' ), admin_url( 'post-new.php' ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

	<p><?php _e( 'Desculpe, mas nada foi encontrado com os termos da sua pesquisa. Por favor, tente novamente com palavras diferentes.', 'FoundationPress' ); ?></p>
	<?php get_search_form(); ?>

	<?php else : ?>

	<p><?php _e( 'Parece que não conseguimos encontrar o que você está procurando.', 'FoundationPress' ); ?></p>
	<?php get_search_form(); ?>

	<?php endif; ?>
</div>
