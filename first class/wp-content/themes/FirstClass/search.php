<?php get_header(); ?>
	
	<div class="row">
		<div class="small-12 large-12 pages" role="main">
		<div class="large-8 medium-8 columns">
			<?php do_action('foundationPress_before_content'); ?>
		
			<h1><?php _e('Busca para:', 'FoundationPress'); ?> "<?php echo get_search_query(); ?>"</h1>
		
			<?php if ( have_posts() ) : ?>
		
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
			
			<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>
			
			<?php endif;?>
			
			<?php do_action('foundationPress_before_pagination'); ?>
			
			<?php if ( function_exists('FoundationPress_pagination') ) { FoundationPress_pagination(); } else if ( is_paged() ) { ?>
				
				<nav id="post-nav">
					<div class="post-previous"><?php next_posts_link( __( '&larr; Anterior', 'FoundationPress' ) ); ?></div>
					<div class="post-next"><?php previous_posts_link( __( 'Próximo &rarr;', 'FoundationPress' ) ); ?></div>
				</nav>
			<?php } ?>
		</div>
		<div class="large-4 medium-4 columns sidebar">
            <?php  get_sidebar(); ?>
		</div>
		<?php do_action('foundationPress_after_content'); ?>

		</div>
	</div>
		
<?php get_footer(); ?>